import React from "react";
import { Orders } from "../../domain/components/orders/Orders";

export const OrdersComp = () => {
  return (
    <>
      <Orders now="New" next="InProgress" button="受注済みにする→" label="受注待ち" />
      <Orders now="InProgress" next="Done" button="発送済みにする→" label="発送待ち" />
    </>
  );
};
