import React from "react";
import { LoginPart } from "../../domain/components/login/LoginPart";
import { useUnLoginChecker } from "../../infra/hooks/useLoginChecker";
import { Register } from "../../domain/components/register/Register";

export const LoginComp = () => {
  useUnLoginChecker();
  return (
    <>
      <LoginPart />
      <Register />
    </>
  );
};
