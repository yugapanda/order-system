import React from "react";
import { Orders } from "../../domain/components/orders/Orders";

export const OrdersHistoriesComp = () => {
  return (
    <>
      <Orders now="Done" next="InProgress" button="←発送待ちにする" label="発送済み" />
    </>
  );
};
