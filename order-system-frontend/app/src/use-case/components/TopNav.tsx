import React from "react";
import { Container, Row, Col } from "reactstrap";

import { useHistory, Link } from "react-router-dom";
import { useGetLoginUserInfo, LoginState } from "../../infra/hooks/useGetLoginUserInfo";
import { User } from "../../infra/models/User";
import strapi from "../../infra/Strapi";
import "./TopNav.css";

export const TopNavComp = () => {
  const userState = useGetLoginUserInfo();
  const history = useHistory();

  const logout = () => {
    strapi.clearToken();
    history.push("/login");
    history.go(0);
  };

  const userOrNot = (nowState: User | LoginState) => {
    if (nowState === LoginState.UN_FETCH) {
      return <Col />;
    }

    if (nowState === LoginState.UN_LOGGED_IN) {
      history.push("/login");
      return <Col />;
    }
    return (
      <>
        <Col shrink style={{ textAlign: "right" }}>
          <span className="btn-nmp">{(nowState as any).username}</span>
          <button type="button" className="btn-nmp" onClick={() => logout()}>
            ログアウト
          </button>
        </Col>
      </>
    );
  };

  return (
    <>
      <Container style={{ height: "100%", marginTop: "2rem" }}>
        <Row className="nav-container">
          <Col className="align-self-center">
            <div className="top-nav-bar">
              <Link to="/order" className="nav-button">
                <span className="nav-word">発注</span>
              </Link>
            </div>
          </Col>
          <Col className="align-self-center">
            <div className="top-nav-bar">
              <Link to="/order/histories" className="nav-button">
                <span className="nav-word">発注履歴</span>
              </Link>
            </div>
          </Col>
          <Col className="align-self-center">
            <div className="top-nav-bar">
              <Link to="/orders" className="nav-button">
                <span className="nav-word">受注処理</span>
              </Link>
            </div>
          </Col>
          <Col className="align-self-center">
            <div className="top-nav-bar">
              <Link to="/orders/histories" className="nav-button">
                <span className="nav-word">受注履歴</span>
              </Link>
            </div>
          </Col>
          <Col className="align-self-center">
            <div className="top-nav-bar">
              <Link to="/product/register" className="nav-button">
                <span className="nav-word">商品登録</span>
              </Link>
            </div>
          </Col>
        </Row>
      </Container>
      <Container>
        <Row>{userOrNot(userState)}</Row>
      </Container>
    </>
  );
};
