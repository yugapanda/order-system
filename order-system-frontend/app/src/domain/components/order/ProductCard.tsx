import { Row, Col } from "reactstrap";
import React, { FC } from "react";
import { Product } from "../../models/Product";

interface ProductCardProps {
  product: Product;
  add: (p: Product) => void;
}

export const ProductCard: FC<ProductCardProps> = ({ product, add }) => {
  return (
    <Row className="table-container">
      <Col className="table-col align-self-center">
        <span className="table-col-cell">{product.name}</span>
      </Col>
      <Col className="table-col align-self-center">
        <span className="table-col-cell">{`${product.price}円`}</span>
      </Col>
      <Col>
        <button type="submit" className="btn-add" onClick={() => add(product)}>
          追加
        </button>
      </Col>
    </Row>
  );
};
