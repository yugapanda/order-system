import { Row, Col } from "reactstrap";
import React, { FC } from "react";

import { OrderDetail } from "../../models/OrderDetail";

interface OrderDetailCardProps {
  orderDetail: OrderDetail;
  change: (order: OrderDetail, num: string) => void;
  del: (p: OrderDetail) => void;
}

export const OrderDetailCard: FC<OrderDetailCardProps> = ({ orderDetail, change, del }) => {
  const num = orderDetail.quantity;
  return (
    <Row className="table-container">
      <Col className="align-self-center table-col-cel">{orderDetail.product.name}</Col>
      <Col className="align-self-center table-col-cel">{`${orderDetail.price}`}</Col>
      <Col className="align-self-center table-col-cel">
        <select value={num} onChange={(e) => change(orderDetail, e.target.value)}>
          {[...Array(100)].map((x, i) => (
            <option>{i}</option>
          ))}
        </select>
      </Col>
      <Col>
        <button type="submit" className="btn-add" onClick={() => del(orderDetail)}>
          削除
        </button>
      </Col>
    </Row>
  );
};
