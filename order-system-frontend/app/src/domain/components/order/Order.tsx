import React, { FC, useState } from "react";
import { Container, Row, Col } from "reactstrap";
import { v4 } from "uuid";
import { cloneDeep } from "lodash-es";
import { useHistory } from "react-router-dom";
import { useGetProducts } from "../../../infra/hooks/useGetProducts";
import { ProductCard } from "./ProductCard";
import { Order as OrderModel } from "../../models/Order";
import { OrderDetail } from "../../models/OrderDetail";
import { Product } from "../../models/Product";
import { OrderDetailCard } from "./OrderDetailCard";
import strapi from "../../../infra/Strapi";
import { sum, sumAddTax } from "../../service/Prices";
import { TAX_RATE } from "../../../infra/Config";

export const Order: FC = () => {
  const [productState] = useGetProducts();
  const [orderState, setOrderState] = useState<OrderDetail[]>([]);

  const history = useHistory();

  const add = (product: Product) => {
    const newOrderState = cloneDeep(orderState);

    const already = newOrderState.find((x) => x.product.id === product.id);

    if (already === undefined) {
      newOrderState.push(new OrderDetail(undefined, product, 1, product.price, TAX_RATE));
      setOrderState(newOrderState);
    } else {
      already.quantity += 1;
      setOrderState(newOrderState);
    }
  };

  const change = (orderDetail: OrderDetail, num: string) => {
    try {
      const quantity = Number.parseInt(num, 10);
      const newOrderState = cloneDeep(orderState);
      const found = newOrderState.find((x) => x.product.id === orderDetail.product.id);

      if (found === undefined) {
        return;
      }
      found.quantity = quantity;
      setOrderState(newOrderState);
    } catch (e) {
      console.log(e);
    }
  };

  const del = (orderDetail: OrderDetail) => {
    const newOrderState = cloneDeep(orderState);
    setOrderState(newOrderState.filter((x) => x.product.id !== orderDetail.product.id));
  };

  const submitOrder = async () => {
    try {
      await strapi.createEntry("orders", new OrderModel(undefined, orderState));
      history.push("/order/histories");
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      <Container>
        {productState.map((x) => {
          return <ProductCard add={add} product={x} key={v4()} />;
        })}
      </Container>

      <Container>
        {orderState.map((x) => {
          return <OrderDetailCard orderDetail={x} change={change} del={del} key={v4()} />;
        })}
        <Row style={{ marginTop: "2rem" }}>
          <Col style={{ textAlign: "right" }}>
            <div>
              合計:
              {sum(orderState)}
            </div>
            <div>
              税込み:
              {
                sumAddTax(
                  orderState,
                  10,
                ) /* TODO: TAXを永続化し、切り上げ丸め等の仕組みもBackend側で決定出来るようにする */
              }
            </div>
            <button type="submit" className="btn-nmp" onClick={() => submitOrder()}>
              発注
            </button>
          </Col>
        </Row>
      </Container>
    </>
  );
};
