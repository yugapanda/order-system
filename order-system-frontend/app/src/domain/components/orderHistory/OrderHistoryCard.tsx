import React, { FC } from "react";

import { Row, Col } from "reactstrap";
import moment from "moment";
import { Order } from "../../../infra/models/Order";
import { OrderDetail } from "../../models/OrderDetail";
import { sum, sumAddTax } from "../../service/Prices";

interface OrderHistoryCardProps {
  order: Order;
}

export const OrderHistoryCard: FC<OrderHistoryCardProps> = ({ order }) => {
  const card = (orderDetail: OrderDetail) => {
    return (
      <Row>
        <Col sm="4" xs="12">
          {orderDetail.product.name}
        </Col>
        <Col sm="4" xs="12">
          {`${orderDetail.price}円`}
        </Col>
        <Col sm="4" xs="12">{`${orderDetail.quantity}個`}</Col>
      </Row>
    );
  };

  return (
    <Row className="table-container">
      <Col xs="12" style={{ textAlign: "right" }}>
        発注日:
        {moment(order.created_at).format("YYYY/MM/DD HH:mm:ss")}
      </Col>
      <Col xs="12">{order.orderDetails.map((x) => card(x))}</Col>
      <Col xs="12" style={{ textAlign: "right" }}>
        合計:
        {sum(order.orderDetails)}
      </Col>
      <Col xs="12" style={{ textAlign: "right" }}>
        税込み:
        {sumAddTax(order.orderDetails, 10)}
      </Col>
    </Row>
  );
};
