import React, { useState } from "react";
import { Container } from "reactstrap";

import { OrderHistoryCard } from "./OrderHistoryCard";
import { useGetOwnOrders } from "../../../infra/hooks/order/useGetOwnOrders";

export const OrderHistory = () => {
  const PAGE_SIZE = 10;
  const [startState, setStartState] = useState(0);
  const orderHistoryState = useGetOwnOrders(startState, PAGE_SIZE);

  return (
    <Container>
      {orderHistoryState.map((x) => (
        <OrderHistoryCard order={x} />
      ))}
    </Container>
  );
};
