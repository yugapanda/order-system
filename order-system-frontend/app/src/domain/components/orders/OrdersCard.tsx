import React, { FC } from "react";
import { Container, Row, Col } from "reactstrap";
import { Order } from "../../../infra/models/Order";
import { OrderHistoryCard } from "../orderHistory/OrderHistoryCard";
import "./Orders.css";

interface OrdersCardProps {
  order: Order;
  doNext: (order: Order) => void;
  button: string;
}

export const OrdersCard: FC<OrdersCardProps> = ({ order, doNext, button }) => {
  return (
    <>
      <Container>
        <Row>
          <Col xs="12" sm="6">
            {order.user.username}
          </Col>
          <Col xs="12" sm="6">
            {order.user.email}
          </Col>
          <Col xs="12">
            <Row className="align-items-center">
              <Col xs="9">
                <OrderHistoryCard order={order} />
              </Col>
              <Col xs="3" className="accept-button">
                <button onClick={() => doNext(order)} type="button" className="btn-nmp">
                  {button}
                </button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
};
