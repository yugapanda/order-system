import React, { useState, FC } from "react";
import { Container, Row, Col } from "reactstrap";
import { useGetAllOrders, updateOrder } from "../../../infra/hooks/order/useGetAllOrders";
import { HttpStatus } from "../../../infra/constant/HttpStatus";
import { OrdersCard } from "./OrdersCard";
import { Order } from "../../../infra/models/Order";

interface OrdersProps {
  now: "New" | "InProgress" | "Done";
  next: "New" | "InProgress" | "Done";
  button: string;
  label: string;
}

export const Orders: FC<OrdersProps> = ({ now, next, button, label }) => {
  const PAGE_SIZE = 10;
  const [startState, setStartState] = useState(0);

  const [ordersNewState, update] = useGetAllOrders(startState, PAGE_SIZE, now);

  const doNext = async (order: Order) => {
    const res = await updateOrder({ ...order, state: next });
    console.log(res);
    update(startState, PAGE_SIZE);
  };

  if (ordersNewState === HttpStatus.NOT_FOUND) {
    return <></>;
  }

  if (ordersNewState === HttpStatus.INTERNAL_SERVER_ERROR) {
    return <></>;
  }

  if (ordersNewState === HttpStatus.PERMISSION_DENIED) {
    return <></>;
  }

  return (
    <Container>
      <Row>
        <Col xs="12" className="text-center">
          <h1>{label}</h1>
        </Col>
      </Row>
      {ordersNewState.map((x) => (
        <OrdersCard order={x} doNext={doNext} button={button} />
      ))}
    </Container>
  );
};
