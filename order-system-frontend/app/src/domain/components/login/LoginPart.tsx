import React, { useState } from "react";
import { Container, Row, Col, Button } from "reactstrap";
import { useHistory } from "react-router-dom";
import strapi from "../../../infra/Strapi";

export const LoginPart = () => {
  const [identifierState, setIdentifierState] = useState("");
  const [passwordState, setPasswordState] = useState("");
  const [loginFailState, setLoginFailState] = useState(false);

  const history = useHistory();

  const login = async (identifier: string, password: string) => {
    try {
      await strapi.login(identifier, password);
      history.push("/home");
      history.go(0);
    } catch (e) {
      console.log(e);
      setLoginFailState(true);
    }
  };

  const loginFail = () => {
    if (loginFailState) {
      return (
        <div style={{ width: "100%", background: "red", textAlign: "center" }}>
          ログインに失敗しました
        </div>
      );
    }
    return <></>;
  };

  return (
    <>
      {loginFail()}
      <Container style={{ marginTop: "2rem" }}>
        <Row>
          <Col xs="12">
            <h1>ログイン</h1>
          </Col>
        </Row>
        <Row>
          <Col xs="12">
            <input
              type="text"
              placeholder="IDかメールアドレス"
              value={identifierState}
              onChange={(e) => setIdentifierState(e.target.value)}
              className="input-nmp"
            />
          </Col>
          <Col xs="12" style={{marginTop: "2rem"}}>
            <input
              type="password"
              placeholder="パスワード"
              value={passwordState}
              onChange={(e) => setPasswordState(e.target.value)}
              className="input-nmp"
              style={{ width: "100%" }}
            />
          </Col>
          <Col xs="12" style={{ textAlign: "right", marginTop: "1rem" }}>
            <Button onClick={() => login(identifierState, passwordState)}>ログイン</Button>
          </Col>
        </Row>
      </Container>
    </>
  );
};
