import React, { FC, useState } from "react";
import { Container, Row, Col, Button } from "reactstrap";
import { useHistory } from "react-router-dom";
import strapi from "../../../infra/Strapi";

export const Register: FC = () => {
  const [nameState, setNameState] = useState("");
  const [mailState, setMailState] = useState("");
  const [passwordState, setPasswordState] = useState("");
  const history = useHistory();

  const register = async (username: string, mail: string, password: string) => {
    try {
      await strapi.register(username, mail, password);
      history.push("/login");
      history.go(0);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      <Container style={{ marginTop: "2rem" }}>
        <Row>
          <Col xs="12">
            <h1>新規登録</h1>
          </Col>
        </Row>
        <Row>
          <Col xs="12" style={{ marginTop: "2rem" }}>
            <input
              style={{ width: "100%" }}
              placeholder="ユーザ名: EX) 発注太郎"
              type="text"
              value={nameState}
              onChange={(e) => setNameState(e.target.value)}
            />
          </Col>
          <Col xs="12" style={{ marginTop: "2rem" }}>
            <input
              style={{ width: "100%" }}
              placeholder="メールアドレス"
              type="mail"
              value={mailState}
              onChange={(e) => setMailState(e.target.value)}
            />
          </Col>
          <Col xs="12" style={{ marginTop: "2rem" }}>
            <input
              style={{ width: "100%" }}
              placeholder="パスワード"
              type="password"
              value={passwordState}
              onChange={(e) => setPasswordState(e.target.value)}
            />
          </Col>
          <Col xs="12" style={{ marginTop: "2rem", textAlign: "right" }}>
            <Button onClick={() => register(nameState, mailState, passwordState)}>新規登録</Button>
          </Col>
        </Row>
      </Container>
    </>
  );
};
