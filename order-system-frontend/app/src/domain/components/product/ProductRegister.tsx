import React, { useState } from "react";
import { Container, Row, Col, Button } from "reactstrap";
import { cloneDeep } from "lodash-es";
import { useHistory } from "react-router-dom";
import { Product } from "../../models/Product";
import strapi from "../../../infra/Strapi";
import { useGetProducts } from "../../../infra/hooks/useGetProducts";

export const ProductRegister = () => {
  const [productsState, setProductsState] = useGetProducts();
  const [productsNewState, setProductsNewState] = useState<Product[]>([]);

  const history = useHistory();

  const changeName = (
    products: Product[],
    set: (p: Product[]) => void,
    index: number,
    value: string,
  ) => {
    const newProduct = cloneDeep(products);
    newProduct[index].name = value;
    set(newProduct);
  };

  const changePrice = (
    products: Product[],
    set: (p: Product[]) => void,
    index: number,
    value: string,
  ) => {
    try {
      const intValue = Number.parseInt(value, 10);
      const newProduct = cloneDeep(products);
      newProduct[index].price = intValue;
      set(newProduct);
    } catch (e) {
      console.log(e);
    }
  };

  const changeUrl = (
    products: Product[],
    set: (p: Product[]) => void,
    index: number,
    value: string,
  ) => {
    const newProduct = cloneDeep(products);
    newProduct[index].url = value;
    set(newProduct);
  };

  const form = (index: number, state: Product[], setState: (p: Product[]) => void) => {
    return <Row className="table-container">
        <Col className="table-col align-self-center" style={{ margin: "1rem" }}>
          <Row style={{ height: "100%"}}>
            <Col xs="12" sm="4">
            <input
              type="text"
              placeholder="商品名"
              className="input-nmp"
              value={state[index].name}
              onChange={(e) => changeName(state, setState, index, e.target.value)}
            />
            </Col>
            <Col xs="12" sm="4">
            <input
              type="text"
              placeholder="価格（円、税別）"
              className="input-nmp"
              value={state[index].price}
              onChange={(e) => changePrice(state, setState, index, e.target.value)}
            />
            </Col>
            <Col xs="12" sm="4">
            <input
              type="text"
              placeholder="商品画像（未実装）"
              className="input-nmp"
              value={state[index].url}
              onChange={(e) => changeUrl(state, setState, index, e.target.value)}
            />
            </Col>
            </Row>
        </Col>
      </Row>
  };

  const update = async (product: Product) => {
    try {
      const updateRes = await strapi.updateEntry("products", product.id.toString(), product);
      console.log(updateRes);
    } catch (e) {
      console.log(e);
    }
  };

  const create = async (product: Product) => {
    try {
      const createRes = await strapi.createEntry("products", product);
      console.log(createRes);
    } catch (e) {
      console.log(e);
    }
  };

  const forms = (products: Product[], newProduct: Product[]) => {
    return (
      <>
        {products.map((x: Product, i: number) => {
          return form(i, productsState, setProductsState);
        })}
        {newProduct.map((x: Product, i: number) => {
          return form(i, productsNewState, setProductsNewState);
        })}
      </>
    );
  };

  const addProduct = () => {
    const newProductsNewState = cloneDeep(productsNewState);
    newProductsNewState.push(new Product(0, "", 0, ""));
    setProductsNewState(newProductsNewState);
  };

  const saveAll = async () => {
    for (let i = 0; i < productsState.length; i += 1) {
      // eslint-disable-next-line no-await-in-loop
      await update(productsState[i]);
    }
    for (let i = 0; i < productsNewState.length; i += 1) {
      // eslint-disable-next-line no-await-in-loop
      await create(productsNewState[i]);
    }
    history.go(0);
  };

  return (
    <Container>
      <Row>
        <Col xs="12">
          <h2>商品一覧</h2>
        </Col>
      </Row>
      <Row className="table-container">
        <Col className="table-col align-self-center" sm="4"><span className="table-col-cell-header">商品名</span></Col>
        <Col className="table-col align-self-center" sm="4"><span className="table-col-cell-header">価格</span></Col>
        <Col className="table-col align-self-center" sm="4"><span className="table-col-cell-header">画像URL</span></Col>
      </Row>
      {forms(productsState, productsNewState)}
      <Row>
        <Col xs="12" className="align-self-center" style={{ textAlign: "right"}}>
          <button type="submit" className="btn-nmp" onClick={() => addProduct()}>新規商品追加</button>
        </Col>

        <Col xs="12" className="align-self-center" style={{ textAlign: "right"}}>
          <button type="submit" className="btn-nmp" onClick={() => saveAll()}>保存</button>
        </Col>
      </Row>
    </Container>
  );
};
