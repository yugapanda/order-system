import { OrderDetail } from "../models/OrderDetail";

export const sum = (orderDetails: OrderDetail[]): number => {
  return orderDetails.map((x) => x.price * x.quantity).reduce((acc, now) => now + acc, 0);
};

export const addTax = (price: number, percentage: number): number => {
  return Math.round(price * (1 + percentage / 100));
};

export const sumAddTax = (orderDetails: OrderDetail[], percentage: number): number => {
  return addTax(sum(orderDetails), percentage);
};
