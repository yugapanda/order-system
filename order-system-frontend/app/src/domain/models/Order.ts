import { OrderDetail } from "./OrderDetail";

export class Order {
  constructor(
    public user: number | undefined,
    public orderDetails: OrderDetail[],
    private tax: number = 10,
    private state: string = "New",
  ) {}
}
