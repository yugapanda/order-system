import { Product } from "./Product";

export class OrderDetail {
  constructor(
    public id: number | undefined,
    public product: Product,
    public quantity: number,
    public price: number,
    public taxRate: number,
  ) {}
}
