import { useEffect, useState } from "react";
import strapi from "../../Strapi";
import { Order } from "../../models/Order";
import { HttpStatus } from "../../constant/HttpStatus";

export const useGetAllOrders = (
  start: number,
  limit: number,
  state: string,
): [Order[] | HttpStatus, (_start: number, _limit: number) => void] => {
  const [ordersState, setOrdersState] = useState<Order[] | HttpStatus>([]);

  const getAllOrders = async (_start: number, _limit: number) => {
    try {
      const allOrders = await strapi.getEntries("orders/all", { _start, _limit, state });
      setOrdersState(allOrders as Order[]);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getAllOrders(start, limit);
  }, []);

  return [ordersState, getAllOrders];
};

export const updateOrder = async (order: Order) => {
  try {
    const res = await strapi.updateEntry("orders", order.id.toString(), order);
    return res;
  } catch (e) {
    console.log(e);
    return false;
  }
};
