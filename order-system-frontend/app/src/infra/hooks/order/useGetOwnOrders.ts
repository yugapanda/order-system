import { useEffect, useState } from "react";
import strapi from "../../Strapi";
import { Order } from "../../models/Order";

export const useGetOwnOrders = (start: number, limit: number): Order[] => {
  const [orderHistoryState, setOrderHistoryState] = useState<Order[]>([]);

  const getOrderState = async (_start: number, _limit: number) => {
    try {
      const orders = await strapi.getEntries("orders", { _start, _limit });
      setOrderHistoryState(orders as Order[]);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getOrderState(start, limit);
  }, []);

  return orderHistoryState;
};
