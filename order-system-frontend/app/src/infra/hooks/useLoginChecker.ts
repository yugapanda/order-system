import { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useGetLoginUserInfo } from "./useGetLoginUserInfo";
import { User } from "../models/User";

export const useUnLoginChecker = () => {
  const userInfo = useGetLoginUserInfo();

  const history = useHistory();

  const loginChecker = () => {
    if (userInfo instanceof User) {
      history.push("/");
    }
  };

  useEffect(() => {
    loginChecker();
  }, [userInfo]);
};
