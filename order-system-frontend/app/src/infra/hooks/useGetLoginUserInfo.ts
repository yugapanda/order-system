import { useEffect, useState } from "react";
import objectAssign from "object-assign";
import strapi from "../Strapi";
import { User } from "../models/User";

export enum LoginState {
  UN_FETCH,
  UN_LOGGED_IN,
}

export const useGetLoginUserInfo = (): User | LoginState => {
  const [userState, setUserState] = useState<User | LoginState>(LoginState.UN_FETCH);

  const getUser = async () => {
    try {
      const userInfo = await strapi.getEntries("users/me");
      const user: User = new User();
      objectAssign(user, userInfo);

      setUserState(user);
    } catch (e) {
      console.log(e);
      setUserState(LoginState.UN_LOGGED_IN);
    }
  };

  useEffect(() => {
    getUser();
  }, []);

  return userState;
};
