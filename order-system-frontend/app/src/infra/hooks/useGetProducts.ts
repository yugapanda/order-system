import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import strapi from "../Strapi";
import { Product } from "../../domain/models/Product";

export const useGetProducts = (): [Product[], (p: Product[]) => void] => {
  const [productsState, setProductsState] = useState<Product[]>([]);
  const history = useHistory();

  const getProducts = async () => {
    try {
      const products = await strapi.getEntries("products");
      setProductsState(products as Product[]);
    } catch (e) {
      if (e.statusCode === 403) {
        history.push("/");
      }
    }
  };

  useEffect(() => {
    getProducts();
  }, []);
  return [productsState, setProductsState];
};
