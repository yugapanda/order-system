import process from "process";

const getEnv = <T>(name: string, defaultVal: T, convert: (s: string) => T): T => {
  const def = process.env[name];

  if (def === undefined) {
    return defaultVal;
  }
  return convert(def);
};

export const TAX_RATE = getEnv<number>("TAX_RATE", 10, Number.parseInt);
