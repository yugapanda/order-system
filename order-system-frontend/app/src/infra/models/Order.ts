import { User } from "./User";
import { OrderDetail } from "../../domain/models/OrderDetail";

export class Order {
  constructor(
    public id: number,
    public user: User,
    public state: "New" | "InProgress" | "Done",
    // eslint-disable-next-line camelcase
    public created_at: Date,
    // eslint-disable-next-line camelcase
    public updated_at: Date,
    public orderDetails: OrderDetail[],
    // eslint-disable-next-line camelcase
    public created_by?: any,
    // eslint-disable-next-line camelcase
    public updated_by?: any,
  ) {}
}
