/* eslint-disable camelcase */
export interface Role {
  id: number;
  name: string;
  description: string;
  type: string;
  created_by?: Date;
  updated_by?: Date;
}

export class User {
  constructor(
    public id?: number,
    public username?: string,
    public email?: string,
    public provider?: string,
    public confirmed?: boolean,
    public blocked?: boolean,
    public role?: Role,
    public created_by?: number,
    public updated_by?: number,
    public created_at?: Date,
    public updated_at?: Date,
  ) {}
}
