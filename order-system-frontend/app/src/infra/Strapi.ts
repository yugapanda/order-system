import Strapi from "strapi-sdk-javascript";
import { env } from "process";

const STRAPI_URL = env.STRAPI;

// eslint-disable-next-line import/no-mutable-exports
let strapi: Strapi;
if (STRAPI_URL === undefined) {
  strapi = new Strapi("http://localhost:1337");
} else {
  strapi = new Strapi(STRAPI_URL);
}

export default strapi;
