import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { TopNavComp } from "./use-case/components/TopNav";
import { HomeComp } from "./use-case/components/Home";

import { OrderComp } from "./use-case/components/Order";
import { LoginComp } from "./use-case/components/Login";
import { UserComp } from "./use-case/components/User";
import { UserIdComp } from "./use-case/components/UserId";
import { OrdersComp } from "./use-case/components/Orders";
import { OrderHistoriesComp } from "./use-case/components/OrderHistories";
import { OrdersHistoriesComp } from "./use-case/components/OrdersHistories";
import { ProductRegisterComp } from "./use-case/components/ProductRegister";

import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/css/bootstrap-reboot.min.css";
import "normalize.css";

function App() {
  return (
    <>
      <BrowserRouter>
        <TopNavComp />
        <div className="body">
          <Switch>
            <Route exact path="/">
              <HomeComp />
            </Route>
            <Route exact path="/order">
              <OrderComp />
            </Route>
            <Route exact path="/order/histories">
              <OrderHistoriesComp />
            </Route>
            <Route exact path="/orders">
              <OrdersComp />
            </Route>
            <Route exact path="/orders/histories">
              <OrdersHistoriesComp />
            </Route>
            <Route exact path="/product/register">
              <ProductRegisterComp />
            </Route>
            <Route exact path="/login">
              <LoginComp />
            </Route>
            <Route exact path="/user">
              <UserComp />
            </Route>
            <Route exact path="/user/:id">
              <UserIdComp />
            </Route>
          </Switch>
        </div>
      </BrowserRouter>
    </>
  );
}

export default App;
