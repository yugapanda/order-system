module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: ["plugin:react/recommended", "airbnb", "plugin:prettier/recommended"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: "module",
  },
  plugins: ["react", "@typescript-eslint", "prettier"],
  rules: {
    quotes: ["error", "double"],
    "@typescript-eslint/no-unused-vars": "error",
    "import/no-unresolved": "off",
    "import/extensions": "off",
    "import/prefer-default-export": "off",
    "react/prop-types": "off",
    "react/jsx-filename-extension": "off",
    "no-useless-constructor": "off",
    "no-empty-function": "off",
    "operator-linebreak": "off",
    "no-console": "off",
    "prettier/prettier": [
      "error",
      {
        printWidth: 100, // 行の最大長 (80),
        trailingComma: "all",
      },
    ],
  },
};
