"use strict";
const { sanitizeEntity } = require("strapi-utils");

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async create(ctx) {
    ctx.request.body.user = ctx.state.user.id;
    const entity = await strapi.services.order.create(ctx.request.body);

    return sanitizeEntity(entity, { model: strapi.models.order });
  },

  /**
   * Update a record.
   *
   * @return {Object}
   */

  async find(ctx) {
    const orders = await strapi.services.order.find({
      "user.id": ctx.state.user.id,
    });

    return orders.map((x) => sanitizeEntity(x, { model: strapi.models.order }));
  },

  async findAll(ctx) {

    const orders = await strapi.services.order.find({
      _start: ctx.request.query._start,
      _limit: ctx.request.query._limit,
      state: ctx.request.query.state,
    });

    return orders.map((x) => sanitizeEntity(x, { model: strapi.models.order }));
  },
};
